﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	public ParticleSystem explosionPrefab;

	void OnDestroy(){
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;

	}
	
	// Update is called once per frame
	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target1;
	public Transform target2;
	public Vector2 heading = Vector3.right; 

	void Update() {
		// get the vector from the bee to the target 
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		float distance1 = direction1.magnitude;
		float distance2 = direction2.magnitude;

		Vector2 currentDirection;
		if (distance1 < distance2) {
			currentDirection = direction1;
		} else {
			currentDirection = direction2;
		}
			

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (currentDirection.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}


		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		//Vector2 direction1 = target.position - transform.position;
		//Gizmos.DrawRay(transform.position, direction1);
	}
}
