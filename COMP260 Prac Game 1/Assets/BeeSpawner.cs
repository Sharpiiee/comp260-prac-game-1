﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	// Use this for initialization
	public int nBees = 50;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	public PlayerMove player1;
	public PlayerMove player2;
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	private float speed;
	private float turnSpeed;
	private BeeSpawner beeSpawner;

	//private Transform target;
	private Vector2 heading;

	private float spawnCountdown = 0.0f;

	void Start () {

	
	}

	public void DestroyBees(Vector2 centre, float radius) {
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			Vector2 v = (Vector2)child.position - centre;
			if(v.magnitude <= radius) {
				Destroy (child.gameObject);


			}
		}
	}


	
	// Update is called once per frame
	void Update () {
		
		spawnCountdown -= Time.deltaTime;


		if (spawnCountdown <= 0.0f) {

			heading = Vector2.right;
			float angle = Random.value * 360;
			heading = heading.Rotate (angle);

			speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
			turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);

			BeeMove bee = Instantiate (beePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "Bee";

			bee.target1 = player1.transform;
			bee.target2 = player2.transform;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y) + Random.insideUnitCircle;


			spawnCountdown = 1f;
		}
	}
}
