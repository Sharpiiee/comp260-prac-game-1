﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	private BeeSpawner beeSpawner;
	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	
	// Update is called once per frame

	public string inputAxisHorizontal = "";
	public string inputAxisVertical = "";

	public float maxSpeed = 5.0f;
	public float destroyRadius =1.0f;


	void Update () {
		if (Input.GetButton ("Fire1")) {
			beeSpawner.DestroyBees (transform.position, destroyRadius);
		}
		Vector2 direction;
		direction.x = Input.GetAxis (inputAxisHorizontal);
		direction.y = Input.GetAxis (inputAxisVertical);

		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);



	




	}
}
